import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import cv2
import sys

cap = cv2.VideoCapture(sys.argv[1])

suc,f = cap.read()

r = f[:,:,0]
g = f[:,:,1]
b = f[:,:,2]

f = f[:,:,::-1]


sys.argv.pop(0)

pos = pd.read_csv(sys.argv[1],header=None).to_numpy()
print(pos.shape)
xs = pos[:,0]
ys = pos[:,1]

plt.imshow(f)
plt.plot(ys,xs,'.-k')

plt.show()
