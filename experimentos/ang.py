import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math

import sys

pos = pd.read_csv(sys.argv[1],header=None).to_numpy()

ax = pos[:,0]
ay = pos[:,1]
bx = pos[:,2]
by = pos[:,3]

dxs = ax - bx
dys = ay - by

angs = []
for i in range(0,len(dxs),3):
    rad = math.atan2(dxs[i],dys[i])
    deg = math.degrees(rad)
    angs.append(deg)

angs = np.array(angs)
angs -= 170
angs[angs < -20] += 360

plt.axhline(y = 0,color='k')
plt.axhline(y = 90,color='k')
plt.axhline(y = 180,color='k')
plt.axhline(y = 270,color='k')
plt.plot(angs,'r.-')
plt.xlabel("Quadros")
plt.ylabel("Direção (º)")
plt.show()
    
