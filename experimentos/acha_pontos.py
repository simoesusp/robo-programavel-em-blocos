import matplotlib.pyplot as plt
from skimage import color, morphology
import numpy as np
import cv2
import sys

cap = cv2.VideoCapture(sys.argv[1])
suc,f = cap.read()
f0 = f
i=0 
SIZE = 60
x = SIZE
y = SIZE

xs = []
ys = []
x1s = []
y1s = []

if len(sys.argv) > 2:
    for j in range(int(sys.argv[2])):
        suc,f = cap.read()
if len(sys.argv) > 4:
    x = int(sys.argv[3])
    y = int(sys.argv[4])
    f = f[x-SIZE:x+SIZE,y-SIZE:y+SIZE]
    
    

def regions(img):
    a = img.copy().astype(np.int32)
    w,h = a.shape
    biggest_region_size = 0
    biggest_region = 1
    region_num = 2
    while np.sum(a==1) > 0:
        seed_x = np.where(a == 1)[0][0]
        seed_y = np.where(a == 1)[1][0]
        queue = [(seed_x,seed_y)]
        region_size = 0
        while len(queue) > 0:
            center = queue.pop()
            a[center] = region_num
            x = center[0]
            y = center[1]
            region_size += 1
            if x >= 1 and a[x-1,y] == 1:
                queue.append((x-1,y))
            if x < w-1 and a[x+1,y] == 1:
                queue.append((x+1,y))
            if y >= 1 and a[x,y-1] == 1:
                queue.append((x,y-1))
            if y < h-1 and a[x,y+1] == 1:
                queue.append((x,y+1))
        if region_size > biggest_region_size:
            biggest_region = region_num
            biggest_region_size = region_size
        region_num += 1
    return a,biggest_region

def get_h(h,s,v):
    reds = np.logical_and(np.abs(h - 0.7) < 0.05,s > 0.4)
    close = morphology.closing(reds,morphology.square(10))
    close1 = morphology.erosion(close,morphology.square(4))
    point = np.logical_and(close, v > 0.7)
    regs,reg = regions(point)

    wh = np.where(regs==reg)
    return int(np.mean(wh[0])) ,int(np.mean(wh[1]))

def get_esp32(h,s,v):
    bris = v > 0.8
    dil1 = morphology.dilation(bris,morphology.disk(5))
    halo = np.logical_and(dil1,s > 0.25)
    halo1 = np.logical_and(halo,h > 0.75)
    close = morphology.dilation(halo1,morphology.square(10))
    bri = np.logical_and(close,v > 0.7)
    regs,reg = regions(bri)
    wh = np.where(regs==reg)
    if len(sys.argv) > 2:
        plt.imshow(v)
        plt.show()
        plt.imshow(bris)
        plt.show()
        plt.imshow(halo1)
        plt.show()
        plt.imshow(bri)
        plt.show()
    return int(np.mean(wh[0])),int(np.mean(wh[1]))

skips = 0
while suc:
    i+=1
    f = color.rgb2hsv(f)

    h = f[:,:,0]
    s = f[:,:,1]
    v = f[:,:,2]

    try:
        x0,y0 = get_esp32(h,s,v)
        x1,y1 = get_h(h,s,v)

        dist_2 = (x0 - x1)**2 + (y0-y1)**2

        x1 += x - SIZE
        y1 += y - SIZE
        x = x +x0 - SIZE
        y = y +y0 - SIZE

        print("%d,%d,%d,%d" % (x,y,x1,y1))
        xs.append(x)
        ys.append(y)
        x1s.append(x1)
        y1s.append(y1)
        SIZE = 60
    except:
        print('Skiping',i,file=sys.stderr)
        skips += 1
        SIZE = 200

    if x < SIZE:
        x = SIZE
    if y < SIZE:
        y = SIZE
        
    suc,f = cap.read()
    if suc:
        f = f[x-SIZE:x+SIZE,y-SIZE:y+SIZE]


print('Skips',skips,file=sys.stderr)
plt.imshow(f0)
plt.plot(ys,xs,'k.-')
plt.plot(y1s,x1s,'r.-')
plt.show()
