#define ADDRESS  0x68
#define GYROCONFIG   0x1B
#define GYRO_XOUT_H  0x43
#define SCALE_GYRO 0x0 //250 DPS
#define RANGE_GYRO 250.0

//velocidade angular
float d_rot_z = 0.0;

//Viés da velocidade angular
float m_rot_z = 0.0;

//Direção atual do robô.
float rot_z = 0.0;

float dt = 0.0;
int count_gyro = 1;

//Lê i2c
uint8_t i2cRead(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* buff) {
  Wire.beginTransmission(Address);
  Wire.write(Register);
  uint8_t result = Wire.endTransmission();
  if (result != 0) {
    return result;
  }

  Wire.requestFrom(Address, Nbytes);
  uint8_t index = 0;
  while (Wire.available()) {
    uint8_t d = Wire.read();
    if (index < Nbytes) {
      buff[index++] = d;
    }
  }
  return 0;
}


//Escreve byte i2c
uint8_t i2cWriteByte(uint8_t Address, uint8_t Register, uint8_t Data) {
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.write(Data);
  return Wire.endTransmission();
}

//Converte 2 bytes em int16
int16_t toInt(uint8_t* buffer, int var){
  return ((int16_t) buffer[var] << 8) | buffer[var+1];
}

//Tarefa do giroscópio
void mpuTask(void* a){

  //Inicia i2c
  Wire.begin(SDA_PIN,SCL_PIN);
  Wire.setClock(1000000);

  //Configura giroscópio
  i2cWriteByte(ADDRESS,0x6B,0x80);
  delay(1000);
  i2cWriteByte(ADDRESS,GYROCONFIG,SCALE_GYRO);

  //Calcula viés (média)
  for(int i = 0; i < 1000; i++){
    uint8_t buffer[6];
    i2cRead(ADDRESS, GYRO_XOUT_H, 6, buffer);
    m_rot_z += (float)-toInt(buffer,4) * RANGE_GYRO / 0x8000 ;;
  }
  m_rot_z /= 1000;

  //Atualiza posição constantemente.
  float last_rot = 0;
  long last_t = micros();
  for(int j = 0; true ; j++){
    uint8_t buffer[6];
    long t  = micros();

    i2cRead(ADDRESS, GYRO_XOUT_H , 6,buffer);
    d_rot_z = (float)-toInt(buffer,4) * RANGE_GYRO / 0x8000 - m_rot_z;
    rot_z += (d_rot_z+last_rot)/2 * (t-last_t) / 1E6;

    dt += (t-last_t) / 1E6;
    last_t = t;
    last_rot = d_rot_z;
  }

  //Encerra tarefa
  vTaskSuspend(NULL);
}

//Cria a tarefa
void startGyro(){
  xTaskCreatePinnedToCore(
    mpuTask,
    "Gyroscope", 
    5000,
    NULL,
    3,
    NULL,
    1
  );
}
