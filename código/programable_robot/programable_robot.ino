#include <ArduinoOTA.h>
#include <Ultrasonic.h>
#include <WiFi.h> 
#include <Wire.h> 

//PINOS USADOS

//Giroscópio
#define SDA_PIN 21
#define SCL_PIN 22

//Ponte-H
#define MOTOR_R 16
#define MOTOR_L 17
#define MOTOR_R_REV 18
#define MOTOR_L_REV 19

//Sonar
#define SONAR_ECHO 26
#define SONAR_TRIG 27

//Led embutido
#define LED 2


//Buffer para Debug
float info[12000];
int info_p = 0;

//Inicia sonar
Ultrasonic ultra(SONAR_TRIG, SONAR_ECHO, 20000UL);

//Outros módulo
#include "gyroscope.h"
#include "control.h"
#include "interpreter.h"
#include "server.h"

void setup() {

  //Inicia modo dos pinos
  pinMode(MOTOR_R,OUTPUT);
  pinMode(MOTOR_L,OUTPUT);
  pinMode(MOTOR_R_REV,OUTPUT);
  pinMode(MOTOR_L_REV,OUTPUT);
  pinMode(LED,OUTPUT);
  pinMode(SONAR_TRIG,OUTPUT);
  pinMode(SONAR_ECHO,INPUT);
  analogWrite(MOTOR_R,0);
  analogWrite(MOTOR_L,0);
  analogWrite(MOTOR_R_REV,0);
  analogWrite(MOTOR_L_REV,0);

  //Inicia task do giroscopio
  startGyro();

  //Cria rede Wi-Fi
  WiFi.softAP("robo","senha123");

  //Inicia arduinoOTA
  ArduinoOTA.begin();

  //Inicia servidor
  startServer();
}

void loop() {
  ArduinoOTA.handle();
}
