const char htmlPage[] PROGMEM = R"rawliteral(

<!DOCTYPE html>
<html>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
<script>

    var cores = ['#0F0','#06F','#06F','#06F','#F00','#F00','#F00','#F0F','#FF0','#FF0']
    var textos = ['INÍCIO',
                  'REPETE |num| VEZES: |cmds|',
                  'SE |exp0| ENTÃO: |cmds|',
                  'ENQUANTO |exp0| FAÇA |cmds|',
                  'FRENTE |num| (CENTESIMO DE SEGUNDO)',
                  'VIRA PARA ESQUERDA',
                  'VIRA PARA DIREITA',
                  'DISTÂNCIA SONAR (CENTIMETROS)',
                  '|exp1| MAIOR QUE |num|',
                  '|exp1| MENOR QUE |num|'
                ]
    var exp_types = ['#FF0','#F0F']

    var menu_map = [] //Mapa de onde pegar coisas no menu
    var mapa_pegar = [] //Mapa de onde pegar as coisas na area de programação
    var mapa_encaixe = [] //Mapa de onde soltar as coisas

    var sel = null; //Bloco sendo pego
    var dx = null; //Diferença da posição do mouse para ponta do bloco pego
    var dy = null; //Diferença da posição do mouse para ponta do bloco pego

    var ult_toque = null; //Último toque touch

    var pilha_grafica = [] //Pilha de figuras para desenhar
    var larguras = [] // Largura das cadeias

    var inputs = 0; //Número de inputs
    var comeco_enquanto = 0; //Comeco do bloco de enquanto
    var menu = null; //Item pego do menu

    var cadeias = [
        {
            x: 100,
            y: 100,
            blocos:[[0,0]]
        }
    ]

    function inverso(a){
        return a.slice().reverse()

    }

    //Verifica se a cor do bloco é expressão
    function retorna(c) {
        return ((c === '#FF0') || (c === '#F0F'))
    }

    //Desenha os componentes da tela
    function esvaziaPilha(ctx){
        while(pilha_grafica.length > 0){
            var desenho = pilha_grafica.pop()
            switch(desenho[0]){
                case 0:
                    x = desenho[1]
                    y = desenho[2]
                    w = larguras[desenho[3]] - x
                    h = desenho[4]
                    c = desenho[5]
                    ctx.strokeRect(x,y,w,h);
                    ctx.fillStyle = c;
                    ctx.fillRect(x,y,w,h);
                    break;
                case 1:
                    x = desenho[1]
                    y = desenho[2]
                    w = desenho[3]
                    h = desenho[4]
                    c = desenho[5]
                    pointyRect(ctx,x,y,w,c)
                    break;
                case 2:
                    x = desenho[1]
                    y = desenho[2]
                    t = desenho[3]
                    ctx.fillStyle = '#000';
                    ctx.fillText(t,x, 35 + y);
                    break;
                case 3:
                    x = desenho[1]
                    y = desenho[2]
                    w = desenho[3]
                    h = desenho[4]
                    c = desenho[5]
                    ctx.strokeRect(x,y,w,h);
                    ctx.fillStyle = c;
                    ctx.fillRect(x,y,w,h);
            }
        }
    }

    //Desenha hexágono
    function pointyRect(ctx,x,y,w,color){
        ctx.beginPath()
        ctx.moveTo(x,y+25)
        ctx.lineTo(x+25,y)
        ctx.lineTo(x+w+25,y)
        ctx.lineTo(x+w+50,y+25)
        ctx.lineTo(x+w+25,y+50)
        ctx.lineTo(x+25,y+50)
        ctx.lineTo(x,y+25)
        ctx.stroke()
        ctx.fillStyle = color;
        ctx.fill()
        ctx.fillStyle = '#000';
        
    }

    //Calcula até que posição pegar
    function achaFim(blocos,i,level){
        if(i >= blocos.length) return 1
        var b = blocos[i][0]
        var j = 1
        var opens = (textos[b].match(/cmds/g) || []).length
        var exps = (textos[b].match(/exp\d+/g) || []).length
        while(exps > 0){
            var nb = blocos[i+j][0]
            if(nb <= 255)
                exps += (textos[nb].match(/exp\d+/g) || []).length
            exps--;
            j++;

        }
        while(opens > level){
            if(i+j >= blocos.length){
                return j+1
            }
            b = blocos[i+j][0]
            if(b == 255){
                opens--
            } else{
                 opens += ((textos[b] || "" ).match(/cmds/g) || []).length
            }
            j++
        }
        return j
    }
        
    //Desenha bloco
    function desenharBloco(c,i,ctx,dx,dy,w){
        if(i >= cadeias[c].blocos.length){
            return [0,w,0]
        }
        var b = cadeias[c].blocos[i][0]
        if(b == 255){
            return [1,w,50]
        }
        if(b > 255){
            var cor = exp_types[b-256]
            mapa_encaixe.push([dx,dy,dx + 80, dy+50,c,i,cor]);
            pilha_grafica.push([1,dx,dy,30,50,cor])
            return [1,30,50]
        }
        var texto = textos[b] 
        var cor = cores[b] 
        var id = cadeias[c].blocos[i][1]
        var dix = 10 
        var diy = 0 
        var ts = texto.split('|')
        var j = 1
        if(retorna(cor)){
            dix += 25
        }
        for( let t of ts){
            switch(t){
                case 'num':
                    form = document.getElementById('num'+id)
                    can = document.getElementById('canvas')
                    form.style.left = can.offsetLeft + (5 + dx + dix)/window.devicePixelRatio + "px" 
                    form.style.top =  can.offsetTop + (dy + 5)/window.devicePixelRatio + "px"
                    dix += 80
                    break
                case t.match('exp')?.input:
                    var ret = desenharBloco(c,i+j,ctx,dx+dix,dy+diy,0)
                    j += ret[0]
                    dix += ret[1] + 60
                    break;
                case 'cmds':
                    var ret = desenharBloco(c,i+j,ctx,dx+50,dy+50,w-40)
                    mapa_encaixe.push([dx,dy+diy,dx + dix,dy+diy+50,c,i+j-1,null]);
                    j += ret[0]
                    diy += ret[2]
                    if(ret[1] + 50 > w) w = ret[1] + 50
                    break
                default:
                    pilha_grafica.push([2,dx+dix,dy+diy,t])
                    dix += ctx.measureText(t).width
                    
            }
            if(dix + 10 > w) w = dix + 10
        }
        diy += 50
        if(retorna(cor)){
            dix -= 25
        }
        var ret = [0,0,0]
        if(retorna(cor)){
            pilha_grafica.push([1,dx,dy,w,diy,cores[b]])
        } else{
            ret = desenharBloco(c,i+j,ctx,dx,dy+diy,w)
            if(ret[1]  > w) w = ret[1]
            mapa_encaixe.push([dx,dy+diy-50,dx + w, dy+diy,c,i+j-1,null]);
            larguras[c] = w + dx
            pilha_grafica.push([0,dx,dy,c,diy,cores[b]])
        }
        mapa_pegar.push([dx,dy,dx + w, dy+diy,c,i]);
        return [j+ret[0],w,diy+ret[2]]
    }

    //Desenha bloco no menu (checar a cadeia)
    function desenharBlocoMenu(b,ctx,dx,dy){
        var texto = textos[b] 
        var cor = cores[b] 
        var dix = 10 
        var diy = 0 
        var ts = texto.split('|')
        if(retorna(cor)){
            dix += 25
        }
        for( let t of ts){
            switch(t){
                case 'num':
                    form = document.getElementById('menu-num'+b)
                    can = document.getElementById('canvas-menu')
                    form.style.left = can.offsetLeft + (5 + dx + dix)/window.devicePixelRatio + "px" 
                    form.style.top = can.offsetTop + (dy + 5)/window.devicePixelRatio  + "px"
                    dix += 80
                    break
                case t.match('exp')?.input:
                    var tipo = parseInt(t.match(/\d+/))
                    var c = exp_types[tipo]
                    pilha_grafica.push([1,dx+dix,dy+diy,30,50,c])
                    dix +=  90
                    break;
                case 'cmds':
                    diy += 50
                    break
                default:
                    pilha_grafica.push([2,dx+dix,dy+diy,t])
                    dix += ctx.measureText(t).width
                    
            }
        }
        diy += 50
        dix += 10
        if(retorna(cor)){
            dix -= 25
        }
        menu_map.push([dx,dy,dx + dix, dy+diy,b]);
        if(retorna(cor)){
            pilha_grafica.push([1,dx,dy,dix,diy,cores[b]])
        } else {
            pilha_grafica.push([3,dx,dy,dix,diy,cores[b]])
        }
        return diy 
    }

    function desenhar(){
        mapa_pegar = []
        mapa_encaixe = []
        canvas = document.getElementById("canvas")    
        ctx = canvas.getContext("2d");
        var escala = window.devicePixelRatio;
        canvas.width = canvas.clientWidth * escala;
        canvas.height = canvas.clientHeight * escala;
        ctx.font = "25px Arial";
        pilha_graficos = []
        var c = cadeias.length - 1
        for(let cad of inverso(cadeias)){
            desenharBloco(parseInt(c),0,ctx,cad.x,cad.y,0)
            c--
        }
        esvaziaPilha(ctx)
    }

    function desenharMenu(){
        diy = 10
        canvas = document.getElementById("canvas-menu")    
        var escala = window.devicePixelRatio;
        canvas.width = canvas.clientWidth * escala;
        canvas.height = canvas.clientHeight * escala;
        ctx = canvas.getContext("2d");
        ctx.font = "25px Arial";
        for(var i = 1; i < textos.length;i++){
            diy += desenharBlocoMenu(i,ctx,10,diy) + 10
            esvaziaPilha(ctx)
        }
    }

    //Acha qual bloco esta na posicao x,y
    function achaBloco(x,y){
        for(let pos of mapa_pegar){
            if(pos[0] <= x && x <= pos[2] && pos[1] <= y && y <= pos[3]){
                return pos;
            }
        }
        return null;
    }

    //Acha qual area de encaixe esta na posicao x,y
    function achaEncaixe(x,y,color){
        for(let pos of mapa_encaixe){
            if(pos[4] != sel &&  pos[0] <= x && x <= pos[2] && pos[1] <= y && y <= pos[3] && color == pos[6]){
                return pos;
            }
        }
        return null;
    }

    function toqueComeco(el,ev){
        if(ev.touches.length == 1){
            var x = Number(ev.touches[0].clientX)
            var y = Number(ev.touches[0].clientY)
            var can = document.getElementById('canvas').getBoundingClientRect()
            var menu = document.getElementById('canvas-menu').getBoundingClientRect()
            if(x > can.left && x < can.right && y > can.top && can.bottom){
                x -= can.left
                y -= can.top
                x *= window.devicePixelRatio
                y *= window.devicePixelRatio
                ult_toque = {'x':x,'y':y}
                pegar(el,ult_toque)
            }
            else if(x > menu.left && x < menu.right && y > menu.top && menu.bottom){
                x -= menu.left
                y -= menu.top
                x *= window.devicePixelRatio
                y *= window.devicePixelRatio
                pegarMenu(el,{'x':x,'y':y})
            }
        }
    }

    function toqueMovimento(el,ev){
        if(ev.touches.length == 1){
            var x = ev.touches[0].clientX
            var y = ev.touches[0].clientY
            var can = document.getElementById('canvas').getBoundingClientRect()
            if(x > can.left && x < can.right && y > can.top && can.bottom){
                x -= can.left
                y -= can.top
                x *= window.devicePixelRatio
                y *= window.devicePixelRatio
                ult_toque = {'x':x,'y':y}
                if(menu != null){
                    entrar(el,ev)
                }
                mover(el,ult_toque)
            }
        } else {
            soltar(el,ult_toque)
        }
    }

    function toqueFim(el,ev){
        if(ev.touches.length == 0){
            soltar(el,ult_toque)
        }
    }

    function pegar(el,ev){
        let x = ev.x - el.offsetLeft 
        let y = ev.y - el.offsetTop
        let pos = achaBloco(x,y)
        if(pos != null){
            if( cadeias[pos[4]].blocos[pos[5]][0] == 255) return
            dx = ev.x - pos[0];
            dy = ev.y - pos[1];
            var end = achaFim(cadeias[pos[4]].blocos,pos[5],-1) - 1
            cadeias.push({
                x:pos[0],
                y:pos[1],
                blocos:cadeias[pos[4]].blocos.slice(pos[5],end + pos[5])
            })
            var cor = cores[cadeias[pos[4]].blocos[pos[5]][0]]
            if(retorna(cor)){
                subs = [exp_types.indexOf(cor) + 256,0]
                cadeias[pos[4]].blocos.splice(pos[5],end,subs)
            }
            else{
                 cadeias[pos[4]].blocos.splice(pos[5],end)
            }
            if(pos[5] == 0) cadeias.splice(pos[4],1)
            sel = cadeias.length - 1;
        }
    }

    function soltar(el,ev){
        if(sel != null){
            var b = cadeias[sel].blocos[0][0]
            var cor = cores[b]
            cor = retorna(cor) ? cor : null 
            let pos = achaEncaixe(ev.x-dx,ev.y-dy,cor)
            if(pos != null && pos[4] != sel){
                if(cor == null){
                    cadeias[pos[4]].blocos.splice(pos[5]+1,0, ...cadeias[sel].blocos)
                } else {
                    cadeias[pos[4]].blocos.splice(pos[5],1, ...cadeias[sel].blocos)
                }
                cadeias.splice(sel,1)
            }
        }
        sel = null;
        menu=null
        desenhar()
    }

    function mover(el,ev){
        if(sel != null){
            var x = ev.x
            var y = ev.y
            cadeias[sel].x = x - dx;
            cadeias[sel].y = y - dy;
            desenhar()
        }
    }

    function entrar(el,ev){
        if(menu != null ){
            var nums = (textos[menu].match(/num/g) || []).length
            next = nums == 0 ? 0 : inputs + 1
            blocos = [[menu,next]]
            for(var i = 0; i < nums; i++){
                inputs++
                var form = document.createElement("input")
                form.id = "num"+inputs
                form.type = 'number'
                form.className = 'num'
                form.value = 1 
                document.getElementById('canvas-area').appendChild(form)
            }
            var exps = (textos[menu].match(/exp\d+/g) || []).map((a) => {return a.match(/\d+/)})
            for(v of exps){
                blocos.push([256+parseInt(v),0])
            }
            var cmdss = (textos[menu].match("/|cmds|/g") || []).length
            for(var i = 0; i < cmdss; i++){
                blocos.push([255,0])
            }
            cadeias.push({
                x: ev.x - el.offsetLeft,
                y: ev.y - el.offsetTop,
                blocos:blocos
            })
            dx = el.offsetLeft
            dy = el.offsetTop
            sel = cadeias.length -1
        }
        menu = null
    }

    function pegarMenu(el,ev){
        let x = ev.x - el.offsetLeft 
        let y = ev.y - el.offsetTop
        for(let pos of menu_map){
            if(pos[0] <= x && x <= pos[2] && pos[1] <= y && y <= pos[3]){
                menu=pos[4]
            }
        }
    }

    function soltarMenu(el,ev){
        menu = null
    }


    function achaComeco(){
        for(c of cadeias){
            if(c.blocos[0][0] == 0){
                return c;
            }
        } 
        return null;
    }



    function compilarBloco(c,i,data){
        var i = i
        if(i >= c.blocos.length){
            return 0
        }
        var b = c.blocos[i][0]
        if(b == 255){
            return 0
        }

        var texto = textos[b]
        var j=1
        var inputs=0

        if(b == 3){ //WHILE
            comeco_enquanto = data.length
        }

        var exps = (texto.match(/exp\d+/g) || [])
        for(var v in exps){
            j+= compilarBloco(c,i+j,data)
        }

        data.push(b)
        var comeco = data.length

        if(b == 3){ //WHILE
            data.push(comeco - comeco_enquanto - 1)
        }

        var nums = (texto.match(/num/g) || [])
        for(var n in nums){
            var id = 'num'+ (c.blocos[i][1] + inputs)
            var val = parseInt(document.getElementById(id).value)
            data.push(val)
            inputs++
        }        

        var cmds = (texto.match(/cmds/g) || [])
        for(var cod in cmds){
            var pos = data.length
            data.push('_')
            j += compilarBloco(c,i+j,data) + 1
            data.push(255)
            data[pos] = data.length - comeco
        }
        
        if(retorna(cores[b])){
           return j
        } else {
            return j + compilarBloco(c,i+j,data)
        } 
        
    }

    function compilar(){
        c = achaComeco()
        if(c == null){
            alert("Nenhum bloco começa com o início");
            return
        }
        data = []
        compilarBloco(c,1,data)
        data.push(254)
        data.splice(0,0,data.length)
        return data
    }
    
    function enviar(){
        data = new Uint8Array(compilar())
        console.log(data)
        var xhr = new XMLHttpRequest();
        xhr.addEventListener("load",function(){
          alert("Codigo enviado!!")
        })
        xhr.open("POST","/code",true);
        xhr.send(data);
        
    }

    function limpar(){
        cadeias = [
            {
                x: 100,
                y: 100,
                blocos:[[0,0]]
            }
        ]
        desenhar()
    }

    function mudarMenu(){
        var abas = document.getElementById('abas')
        var canvas = document.getElementById('canvas-area')
        if(abas.style.display != 'none'){
            abas.style.display = 'none'
            canvas.style.width = '100vw'
            desenhar()
        } else {
            abas.style.display = 'block'
            canvas.style.width = '65vw'
            desenhar()
            desenharMenu()
        }
    }
</script>
<style>
    body, html{
        background:#DDD;
        width:100%;
        height:100%;
        margin:0px;
        padding:0px;
    }
    #botoes{
        background:#DDD;
        width:96vw;
        padding-top: 2vh;
        padding-bottom: 2vh;
        padding-left: 2vw;
        padding-right: 2vw;
    }
    #abas{
        display:block;
        background:#DDD;
        width:34vw;
        height:90vh;
        float:left;
    }
    canvas{
        width:100%;
        height:100%;
    }
    #canvas-area{
        background:#FFF;
        width:65vw;
        height:90vh;
        float:right;
        overflow: none;
    }
    #canvas-area-menu{
        background:#FFF;
        width:90%;
        margin-left:2vh;
        height:100%;
        overflow: none;
    }
    button{
        background-color:#FC0;
        margin-right: 5vh;
        padding-right: 2vh;
        padding-left: 2vh;
        border-radius: 2vh;
        font-size: 3vh;
    }
    #send{
        background-color:#0F0;
    }
    .num{
        position: absolute;
        width: 7vh;
        height: 3vh;
        font-size: 3vh;
    }
</style>
<body onresize=desenhar();desenharMenu()
    ontouchstart=toqueComeco(this,event,true)
    ontouchend=toqueFim(this,event,true)
    ontouchmove=toqueMovimento(this,event,true)>
    <div id="botoes">
        <button id=send onclick=enviar()>Enviar Código</button>
        <button onclick=mudarMenu()>Esconder/Mostrar Menu</button>
        <button onclick=document.body.requestFullscreen()>Tela Cheia</button>
    </div>
    <div>
    <div id="abas">
        <div id="canvas-area-menu"
            onmouseup=soltarMenu(this,event)>
            <canvas id="canvas-menu" onmousedown=pegarMenu(this,event)>
            </canvas>
            <input id="menu-num1" class="num" type="number" >
            <input id="menu-num4" class="num"type="number">
            <input id="menu-num8" class="num"type="number">
            <input id="menu-num9" class="num"type="number">
        </div>
    </div>
    <div id="canvas-area"
        onmousemove=mover(this,event)
        onmouseup=soltar(this,event)
        onmouseleave=soltar(this,event)
        onmouseenter=entrar(this,event)> 
        <canvas id="canvas" onmousedown=pegar(this,event)>
        </canvas>
    </div>
    </div>
    <script>
        desenharMenu();
        desenhar();
    </script>
</body>
</html>
)rawliteral";
