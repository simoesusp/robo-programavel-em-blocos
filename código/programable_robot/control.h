#define KP 1
#define KI 0.002
#define KD 15

float goal = 0.0;

void virarEsquerda(int graus){
  analogWrite(MOTOR_R,150);
  analogWrite(MOTOR_L_REV,150);
  float ultErro = 90;
  goal -= 90;
  int i = 0;
  while(goal < rot_z){
    float erro = rot_z - goal;
    int vel = 150 + (int)((erro-ultErro)*100);
    analogWrite(MOTOR_R,vel);
    analogWrite(MOTOR_L_REV,vel);
    delay(5);
    ultErro = erro;
    i++;
  }
  analogWrite(MOTOR_R,0);
  analogWrite(MOTOR_L_REV,0);
  delay(200);
}

void virarDireita(int graus){
  analogWrite(MOTOR_L,150);
  analogWrite(MOTOR_R_REV,150);
  float ultErro = 90;
  goal += 90;
  while(goal > rot_z){
    float erro = goal - rot_z;
    int vel = 150 + (int)((erro-ultErro)*100);
    analogWrite(MOTOR_L,vel);
    analogWrite(MOTOR_R_REV,vel);
    delay(5);
    ultErro = erro;
  }
  analogWrite(MOTOR_L,0);
  analogWrite(MOTOR_R_REV,0);
  delay(200);
}

void frente(int tempo){
  long inicio = micros();
  float somaErro = 0;
  float ultErro = 0;
  while(micros() - inicio < tempo * 10000){
    float erro = goal-rot_z;
    somaErro += erro;
    float diff = erro*KP + somaErro*KI + (erro-ultErro)*KD;
    analogWrite(MOTOR_L,150+(int)diff);
    analogWrite(MOTOR_R,150-(int)diff);                                                                                                 
    delay(5);
    ultErro = erro;
  }
  analogWrite(MOTOR_R,0);
  analogWrite(MOTOR_L,0);
  delay(200); 
}
