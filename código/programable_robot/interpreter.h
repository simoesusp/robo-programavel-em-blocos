#define MAX_ITER 100

#define REPEAT 1
#define IF 2
#define WHILE 3
#define FRONT 4
#define LEFT 5
#define RIGHT 6
#define SONAR 7
#define MORE 8
#define LESS 9


int pc = 0;
uint8_t sp = 0;
uint8_t stack[256];
char code[256];

bool available = true;

void push(uint8_t val){
  stack[sp] = val;
  sp++;
}

uint8_t pop(){
  sp--;
  uint8_t ret = stack[sp];
  return ret;
}

void interpretProgram(void* a){
  pc=0;
  sp=0;
  rot_z = 0;
  info_p = 0;
  goal = 0;
  digitalWrite(LED,HIGH);
  for(int i = 0; i < MAX_ITER; i++){
    uint8_t inst = code[pc];
    info[info_p] = (float) pc;
    info_p ++;
    info[info_p] = (float) inst;
    info_p ++;
    if(inst == REPEAT){ //REPEAT n TIMES code:
        int n = (int) code[pc+1];
        int jmp = code[pc+2];
        push(pc+jmp+1);
        for(int j = 1;j < n;j++) push(pc+3);
        pc += 3;
    }
    else if(inst == FRONT){ //FRONT FOR n SECONDS
        int n = (int) code[pc+1];
        frente(n);
        pc += 2;
    }
    else if(inst == LEFT){ //LEFT TURN
        virarEsquerda(90);
        pc++;
    }
    else if(inst == RIGHT){ //RIGHT TURN
        virarDireita(90);
        pc++;
    }
    else if(inst == SONAR){ //READ SONAR
        int dist = ultra.read(CM);
        dist = dist > 255 ? 255 : dist;
        push(dist);
        pc++;
    }
    else if(inst == MORE){ // VAL MORE THAN N
        int val = pop();
        int num = code[pc+1];
        uint8_t ret = val > num ? 1 : 0;
        push(ret);
        pc+=2;
    }
    else if(inst == LESS){ // VAL LESS THAN N
        int val = pop();
        int num = code[pc+1];
        uint8_t ret = val < num ? 1 : 0;
        push(ret);
        pc+=2;
    }
    else if(inst == IF){ // IF-THEN
      int val = pop();
      int jmp = code[pc+1];
      if(val != 0){
        push(pc+jmp+1);
        pc+=2;
      }
      else{
        pc += jmp+1;
      }
    }
    else if(inst == WHILE){ // WHILE
      int val = pop();
      int jmp1 = code[pc+1];  
      int jmp2 = code[pc+2];
      if(val != 0){
        push(pc-jmp1);
        pc+=3;
      }
      else{
        pc += jmp2+1;
      }
    }
    else if(inst == 255){ //End block
        pc = pop();
    }
    else{ //Halt
      digitalWrite(LED,LOW);
      available = true;
      vTaskSuspend(NULL);
      return; //Testar
    }
  }
  available = true;
  vTaskSuspend(NULL);
}

void interpretTask(){
  if(available){
    available = false;
    xTaskCreatePinnedToCore(
      interpretProgram,
      "Interpreter", 
      5000,
      NULL,
      2,
      NULL,
      0
    );
  }
}


