#include "esp_http_server.h"
#include "page.h"

httpd_handle_t server = NULL;

esp_err_t handleInfo(httpd_req_t *r){
  httpd_resp_send(r,(char*)info,12000*4);
  return ESP_OK;
}

esp_err_t handlePage(httpd_req_t *r){
  httpd_resp_send(r,htmlPage,strlen(htmlPage));
  return ESP_OK;
}

esp_err_t handleCode(httpd_req_t *r){
  char len;
  if(httpd_req_recv(r,&len,1) != 1){
    return ESP_OK;
  }
  if(httpd_req_recv(r,code,len) == len){
    httpd_resp_set_hdr(r, "Connection","close");
    httpd_resp_send(r,"ok",2);
    interpretTask();
  } else {
    httpd_resp_set_hdr(r, "Connection","close");
    httpd_resp_send(r,"erro",4);
  }
  
  return ESP_OK;
}

void startServer(){
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();
  config.server_port = 80;

  httpd_uri_t uriPage = {
    .uri       = "/",
    .method    = HTTP_GET,
    .handler   = handlePage,
    .user_ctx  = NULL
  };

  httpd_uri_t uriInfo = {
    .uri       = "/info",
    .method    = HTTP_GET,
    .handler   = handleInfo,
    .user_ctx  = NULL
  };

  httpd_uri_t uriCode = {
    .uri       = "/code",
    .method    = HTTP_POST,
    .handler   = handleCode,
    .user_ctx  = NULL
  };

  if (httpd_start(&server, &config) == ESP_OK) {
    httpd_register_uri_handler(server, &uriPage);
    httpd_register_uri_handler(server, &uriInfo);
    httpd_register_uri_handler(server, &uriCode);
  }
}
